import React, {Component} from 'react';
import AppNavigator from './navigation/AppNavigator';
import LoginContainer from './navigation/LoginContainer';
import AsyncStorage from '@react-native-community/async-storage';


class AppContainer extends Component {
    _isMounted = false;
    state = {
        isLogged: false
    }

    async componentDidMount(){
        this._isMounted = true;
        try {
           const details = await AsyncStorage.getItem('profile');
           if (this._isMounted) {
               if (details != null){
                    this.setState({
                        isLogged: true
                    })
               }else{
                   this.setState({
                       isLogged:false
                   })
               }
           }
        } catch (error) {
            console.log(error)
        }
    }

    async componentDidUpdate(){
        this._isMounted = true;
        try {
           const details = await AsyncStorage.getItem('@profile');
           if (this._isMounted) {
               if (details != null){
                    this.setState({
                        isLogged: true
                    })
               }else{
                   this.setState({
                       isLogged:false
                   })
               }
           }
        } catch (error) {
            console.log(error)
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
    

    render() {
        return this.state.isLogged ? (
            <AppNavigator />
        ) : <LoginContainer />
    }
} 
      
export default AppContainer