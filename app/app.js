/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import configureStore from '../store';
import React, {useEffect} from 'react';
// import {AppIconsPack} from './app-icons-pack';
import Loading from '../components/RefreshIcon';
import {Theming} from '../services/theme.service';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {appMappings, appThemes} from './app-theming';
import {AppNavigator} from '../navigations/app.navigator';
import {PersistGate} from 'redux-persist/integration/react';
import {GoogleSignin} from '@react-native-community/google-signin';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';

// import { AppearanceProvider } from 'react-native-appearance';
import SplashScreen from 'react-native-splash-screen';

const defaultConfig = {
  mapping: 'eva',
  theme: 'light',
};
const {store, persistor} = configureStore();
const App = () => {
  const [mappingContext, currentMapping] = Theming.useMapping(
    appMappings,
    defaultConfig.mapping,
  );
  const [themeContext, currentTheme] = Theming.useTheming(
    appThemes,
    defaultConfig.mapping,
    defaultConfig.theme,
  );
  useEffect(() => {
    SplashScreen.hide();
    GoogleSignin.configure({
      webClientId:
        '1050394671930-8gakgg42vh9ivid8a4qh3t90np8333eo.apps.googleusercontent.com',
      offlineAccess: true,
    });
  }, []);
  return (
    <Provider store={store}>
      <PersistGate loading={<Loading />} persistor={persistor}>
        <IconRegistry icons={EvaIconsPack} />
        <ApplicationProvider {...currentMapping} theme={currentTheme}>
          <Theming.MappingContext.Provider value={mappingContext}>
            <Theming.ThemeContext.Provider value={themeContext}>
              <AppNavigator />
            </Theming.ThemeContext.Provider>
          </Theming.MappingContext.Provider>
        </ApplicationProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
