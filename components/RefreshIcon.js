import React, {Component} from 'react';
import {View, StyleSheet, AppRegistry, ActivityIndicator} from 'react-native';
import Color from '../constant';

export default class RefreshIcon extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator
          size={30}
          color={Color.tintColor}
          style={styles.loading}
        />
      </View>
    );
  }
}

AppRegistry.registerComponent('RefreshIcon', () => RefreshIcon);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  loading: {
    elevation: 4,
    marginTop: 5,
  },
});
