import React, {Component} from 'react';
import {View, Platform, TextInput, StyleSheet} from 'react-native';

export default class SearchBox extends Component {
  render() {
    return (
      <View
        style={
          this.props.isHome
            ? styles.containerPadding
            : styles.containerNoPadding
        }>
        <View style={styles.searchcontainer}>
          <TextInput
            placeholder="Search by topic e.g. algebra"
            placeholderTextColor="rgba(14,17,17,0.7)"
            style={styles.searchInput}
            editable={this.props.isHome ? false : true}
            autoFocus={this.props.isHome ? false : true}
            inlineImageLeft={Platform.OS === 'ios' ? 'ios-search' : 'md-search'}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerPadding: {
    flex: 1,
    padding: 10,
  },
  containerNoPadding: {
    flex: 1,
    padding: 0,
  },
  searchcontainer: {
    width: 325,
  },
  searchInput: {
    backgroundColor: '#E6E8E9',
    padding: 7,
    borderRadius: 4,
    // marginVertical: 10,
    // paddingHorizontal: 10,
    fontSize: 17,
  },
});
