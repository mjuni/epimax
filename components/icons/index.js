import React from 'react';
import { Icon } from '@ui-kitten/components';

export const ArrowBackIcon = (style) => (
    <Icon {...style} name='arrow-ios-back' />
);
export const AssetEcommerceIcon = (style) => (
    <Icon {...style} pack='app' name='ecommerce' />
);

export const AssetEcommerceDarkIcon = (style) => (
    <Icon {...style} pack='app' name='ecommerce-dark' />
);
export const MenuIcon = (style) => (
    <Icon {...style} name='menu' />
);

export const StarIcon = (style) => (
    <Icon {...style} name='star' />
);

export const StarOutlineIcon = (style) => (
    <Icon {...style} name='star-outline' />
);
export const LayoutIcon = (style) => (
    <Icon {...style} name='layout-outline' />
);
export const MoreVerticalIcon = (style) => (
    <Icon {...style} name='more-vertical' />
);
export const PlusOutlineIcon = (style) => (
    <Icon {...style} name='plus-outline' />
);
export const PlusCircleOutlineIcon = (style) => (
    <Icon {...style} name='plus-circle-outline' />
);
export const ShoppingBagOutlineIcon = (style) => (
    <Icon {...style} name='shopping-bag-outline' />
);
export const EditOutline = (style) => (
    <Icon {...style} name='edit-outline' />
);
export const CheckMarkOutline = (style) => (
    <Icon {...style} name='checkmark-outline' />
);
export const CloseOutline = (style) => (
    <Icon {...style} name='close-outline' />
);
export const BookmarkOutline = (style) => (
    <Icon {...style} name='bookmark-outline' />
);
export const SettingsOutlineIcon = (style) => (
    <Icon {...style} name='settings-2-outline' />
);
export const FaceboolOutlineIcon = (style) => (
    <Icon {...style} name='facebook-outline' />
);