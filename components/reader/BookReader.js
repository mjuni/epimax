import React, {useState, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import DrawerLayout from 'react-native-gesture-handler/DrawerLayout';
import {ArrowBackIcon, MenuIcon} from '../../components/icons';
import {DrawerAccessoriesShowcase} from './Drawer';
import {
  Layout,
  TopNavigation,
  TopNavigationAction,
} from '@ui-kitten/components';
import {WebView} from 'react-native-webview';
import Spinner from 'react-native-loading-spinner-overlay';

export default function ({currentTopicIndex, topics, subject, ...drawerProps}) {
  let drawer = null;
  const [spinnerVisible, setSpinnerVisible] = useState(false);
  const [selectedTopicIndex, setSelectedTopicIndex] = useState(
    currentTopicIndex ? currentTopicIndex : 0,
  );
  const [selectedTopic, setSelectedTopic] = useState(
    topics[selectedTopicIndex],
  );
  const [currentUrl, setCurrentUrl] = useState(null);

  const openDrawer = () => {
    drawer.openDrawer();
  };
  const createUrl = () => {
    const {level_id} = selectedTopic;
    // const filename = `t0${selectedTopicIndex + 1}.html`;
    const filename = `${selectedTopic.notes}.html`;
    const url = `https://epimaxtz.netlify.app/sub/${level_id}/${subject.name}/${filename}`;
    console.log(url);
    return url;
  };
  const changeTopic = () => {
    const url = createUrl();
    setCurrentUrl(url);
  };
  const handleSelect = (index) => {
    try {
      const topic = topics[index];
      setSelectedTopic(topic);
      setSelectedTopicIndex(index);
    } catch (error) {}
  };
  const showSpinner = () => {
    drawer.closeDrawer();
    setSpinnerVisible(true);
  };

  const hideSpinner = () => {
    setSpinnerVisible(false);
  };
  const renderBackAction = () => <TopNavigationAction icon={ArrowBackIcon} />;
  const renderRightAction = () => (
    <TopNavigationAction icon={MenuIcon} onPress={openDrawer} />
  );

  const renderDrawer = () => {
    return (
      <Layout style={styles.container}>
        <DrawerAccessoriesShowcase
          currentTopicIndex={currentTopicIndex}
          subject={subject}
          topics={topics}
          {...drawerProps}
          onSelect={handleSelect}
        />
      </Layout>
    );
  };
  useEffect(() => {
    changeTopic();
  }, [selectedTopic]);

  return (
    <>
      <Spinner
        visible={spinnerVisible}
        textContent={'Loading...'}
        textStyle={styles.spinner}
      />
      <DrawerLayout
        ref={(ref) => (drawer = ref)}
        drawerWidth={200}
        drawerPosition={DrawerLayout.positions.Right}
        drawerType="front"
        drawerBackgroundColor="#ddd"
        renderNavigationView={renderDrawer}>
        <>
          <TopNavigation
            accessoryLeft={renderBackAction}
            accessoryRight={renderRightAction}
          />
          {currentUrl ? (
            <WebView
              source={{uri: currentUrl}}
              onLoadStart={showSpinner}
              onLoad={hideSpinner}
            />
          ) : null}
        </>
      </DrawerLayout>
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  spinner: {
    color: '#FFF',
  },
  renderDrawer: {
    flex: 1,
  },
});
