import React from 'react';
import {View, StyleSheet} from 'react-native';
import FastImage from 'react-native-fast-image';
import {Divider, Drawer, DrawerItem, Text} from '@ui-kitten/components';

const Header = (props) => (
  <React.Fragment>
    <FastImage
      style={[props.style, styles.header]}
      source={{uri: props.subject.image_url}}
    />
    <View style={styles.headerContent}>
      <Text style={styles.title}>{props.subject.name}</Text>
      <Text style={styles.title}>Form one</Text>
    </View>
    <Divider />
  </React.Fragment>
);

export const DrawerAccessoriesShowcase = ({
  subject,
  level,
  currentTopicIndex,
  topics,
  onSelect,
}) => {
  const [selectedIndex, setSelectedIndex] = React.useState({
    row: currentTopicIndex,
  });
  const handleSelect = (selected) => {
    setSelectedIndex(selected);
    const {row} = selected;
    console.log(row);;
    onSelect(row);;
  };;
  return (
    <Drawer
      header={(props) => <Header subject={subject} level={level} {...props} />}
      selectedIndex={selectedIndex}
      onSelect={handleSelect}>
      {topics.map((topic, index) => (
        <DrawerItem title={topic.name} key={topic.id} />
      ))}
    </Drawer>
  );
};

const styles = StyleSheet.create({
  header: {
    width: 140,
    height: 180,
    margin: 20,
  },
  headerContent: {
    marginBottom: 10,
  },
  title: {
    textAlign: 'center',
  },
});
