import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {getCurrentUser} from '../slices';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {LayoutsNavigator} from './layouts.navigator';
import {LibraryNavigator} from './library.navigator';
import {SettingNavigator} from './setting.navigator';
import SplashScreen from '../screens/SplashScreen';
import LoginScreen from '../screens/login';
import {HomeBottomNavigation} from '../screens/home/home-bottom-navigation';

const BottomTab = createBottomTabNavigator();
const Stack = createStackNavigator();
// const Drawer = createDrawerNavigator();

const initialTabRoute = 'Home';

const ROOT_ROUTES = ['Home', 'Library', 'Setting'];

const isOneOfRootRoutes = (currentRoute) => {
  return ROOT_ROUTES.find((route) => currentRoute.name === route) !== undefined;
};

const TabBarVisibleOnRootScreenOptions = ({route}) => {
  const currentRoute = route.state && route.state.routes[route.state.index];
  return {tabBarVisible: currentRoute && isOneOfRootRoutes(currentRoute)};
};

export const HomeNavigator = () => {
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.auth.isLoading);
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  console.log({isAuthenticated, isLoading});
  useEffect(() => {
    // dispatch(getCurrentUser());
  }, []);
  if (isLoading) {
    console.log({isLoading});
    return (
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="Splash" component={SplashScreen} />
      </Stack.Navigator>
    );
  }
  return isAuthenticated ? (
    <BottomTab.Navigator
      screenOptions={TabBarVisibleOnRootScreenOptions}
      initialRouteName={initialTabRoute}
      tabBar={(props) => <HomeBottomNavigation {...props} />}>
      <BottomTab.Screen name="Home" component={LayoutsNavigator} />
      <BottomTab.Screen name="Library" component={LibraryNavigator} />
      <BottomTab.Screen name="Setting" component={SettingNavigator} />
    </BottomTab.Navigator>
  ) : (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Login" component={LoginScreen} />
    </Stack.Navigator>
  );
};
