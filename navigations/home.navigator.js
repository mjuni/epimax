import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/home';
import TopicScreen from '../screens/topics';
import ReaderScreen from '../screens/reader';

const Stack = createStackNavigator();
export const HomeNavigator = () => (
  <Stack.Navigator headerMode="none">
    <Stack.Screen name="Home" component={HomeScreen} />
    <Stack.Screen name="Topics" component={TopicScreen} />
    <Stack.Screen name="Reader" component={ReaderScreen} />
  </Stack.Navigator>
);
