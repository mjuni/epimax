import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
// import {HomeNavigator} from './home.navigator';
import HomeScreen from '../screens/home';
import TopicScreen from '../screens/topics';
import ReaderScreen from '../screens/reader';
import AboutScreen from '../screens/about';

const Stack = createStackNavigator();
export const LayoutsNavigator = () => (
  <Stack.Navigator headerMode="none">
     {/* <Stack.Screen name="Layouts" component={HomeNavigator} /> */}
    <Stack.Screen name="Home" component={HomeScreen} />
    <Stack.Screen name="Topics" component={TopicScreen} />
    <Stack.Screen name="Reader" component={ReaderScreen} />
    <Stack.Screen name="About" component={AboutScreen} />
  </Stack.Navigator>
);
