import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import LibraryScreen from '../screens/library';
import ReaderScreen from '../screens/reader';

const Stack = createStackNavigator();
export const LibraryNavigator = () => (
  <Stack.Navigator headerMode="none">
    <Stack.Screen name="Library" component={LibraryScreen} />
    <Stack.Screen name="Reader" component={ReaderScreen} />
  </Stack.Navigator>
);