import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import AboutScreen from '../screens/about';
import SettingScreen from '../screens/setting';

const Stack = createStackNavigator();
export const SettingNavigator = () => (
  <Stack.Navigator headerMode="none">
    <Stack.Screen name="Setting" component={SettingScreen} />
    <Stack.Screen name="About" component={AboutScreen} />
  </Stack.Navigator>
);