import {combineReducers} from '@reduxjs/toolkit';
import SubjectReducer from './slices/subjectSlice';
import TopicReducer from './slices/topicsSlice';
import LevelReducer from './slices/levelSlice';
import LibraryReducer from './slices/librarySlice';
import AuthReducer from './slices/AuthSlice';

const rootReducer = combineReducers({
  auth: AuthReducer,
  level: LevelReducer,
  subject: SubjectReducer,
  topic: TopicReducer,
  library: LibraryReducer,
});
export default rootReducer;
