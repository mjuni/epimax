import React, {Component} from 'react';
import { 
    StyleSheet,
    View,
    Text,
    Image,
   } from 'react-native';

export default class AboutScreen extends Component {
    render() {
        return(
            <View style={styles.container}>
                <Image 
                    source={require('../assets/images/logo.jpeg')}
                    style={{ height: 50, width: 300 }}
                />
                <Text style={styles.logoText}>Epimax Plus</Text>
                <Text style={styles.version}>Version: 0.01</Text>
                <View style={styles.footer}>
                    <Text style={styles.footerText}>Copyright {'\u00A9'} Epimax Plus All Rights Reserved</Text>
                </View>
            </View>
        )
    }            
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    logoText: {
        alignSelf: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        paddingTop: 25,
    },
    version: {
        alignSelf: 'center',
        fontSize: 16,
        paddingTop: 25,
        color: 'rgb(128,128,128)'
    },
    footer: {
        position:'absolute',
        bottom: 15,
    },
    footerText: {
        fontSize: 14,
        bottom: 0,
        color: 'rgb(128,128,128)',
    }
});