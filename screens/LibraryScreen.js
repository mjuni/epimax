import React, { Component } from 'react';
import LibrarySection from "../sections/LibrarySection";
import AsyncStorage from '@react-native-community/async-storage';
import {
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  ScrollView,
} from 'react-native';

export default class LibraryScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      levels: [],
    }
  }

  async fetchLevels() {
    try {
      const levels = await AsyncStorage.getItem('@levels');
      if(levels !== null) {
        this.setState({
          levels: JSON.parse(levels),
        })
      }
    } catch(error) {
      console.log(error)
    }
  }

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener("didFocus", () => {
      this.fetchLevels();
    });
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  render() {
    const state = this.state
    return (
      <ScrollView style={ styles.container }>
          <View style={styles.scrollView}>
              { 
                state.levels.length > 0 ? (
                  state.levels.map(
                    level => <LibrarySection {...this.props} level={level.name} level_id={level.id} key={level.id}/>
                  )
                ) : <Text>Nothing in Here. Save topics to view them here.</Text>
              }
          </View>
      </ScrollView>
    );
  }
}


const bookCover = {
    height: 140,
    width: 100
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  scrollView: {
    paddingTop: 25,
    paddingLeft: 25,
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: bookCover.height,
    width: bookCover.width
  },
  bookTitle: {
      paddingTop: 8,
      fontWeight: 'bold',
      fontSize: 15,
      width: bookCover.width,
  },
});