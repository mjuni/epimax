import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
//  import * as Font from 'expo-font';
import Color from '../constants/Colors';
import logo from '../assets/images/logo.jpeg';
import Icon from 'react-native-vector-icons/Ionicons';
import {googleAuth} from '../actions/authenticationAction';
import { 
  StyleSheet,
  Text,
  View,
  Image,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import { 
  Portal, 
  Modal,
  TouchableRipple
} from "react-native-paper";

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.signInWithGoogleAsync = this.signInWithGoogleAsync.bind(this)
  }
  _isMounted = false;
    
  state = {
      fontLoaded: false,
      visible: false
  };
    
  async componentDidMount() {
    this._isMounted = true;
    // await Font.loadAsync({
    //   'Poppins-Regular': require('../assets/fonts/Poppins/Poppins-Regular.ttf'),
    // });
    if (this._isMounted) {
      this.setState({ fontLoaded: true});
    }
  }

  async signInWithGoogleAsync() {
    this.setState({ visible:true })
    this.props.googleAuth()
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  _hideModal = () => this.setState({ visible: false });

  render() {
    const { visible } = this.state;
    return this.state.fontLoaded ? (
        <Portal>
          <View style={styles.container}>
            <View style={styles.logoContainer}>
            <Image source={logo} fadeDuration={0} style={{ height: 50, width: 300 }} />
            </View>
            <View style={styles.loginHeader}>
              <Text style={styles.loginHeaderText}>Easy Login</Text>
            </View>
            <View style={styles.loginContainer}>
              <TouchableRipple onPress={() => this.props.navigation.navigate('AppNavigator')} style={styles.facebookButton}>
                <View style={styles.nestedButtonView}>
                  <Icon name="logo-facebook" size={32} color="white"/>
                  <Text style={styles.facebookButtonText}>Log in with Facebook</Text>
                </View>
              </TouchableRipple>
              <TouchableRipple onPress={() => this.signInWithGoogleAsync()} style={styles.googleButton}>
                <View style={styles.nestedButtonView}>
                    <Image
                      source={require('../assets/images/google-icon.png')}
                      fadeDuration={0}
                      style={{width: 30, height: 30}}
                    />
                    <Text style={styles.googleButtonText}>Log in with Google</Text>
                </View>
              </TouchableRipple>
            </View>
          </View>
          <Modal visible={visible} onDismiss={this._hideModal}>
            <View style={styles.loading}>
              <View style={styles.nestedButtonView}>
                <ActivityIndicator size={55} color={Color.tintColor} />
                <Text style={styles.loadingText}>Please Wait...</Text>
              </View>
            </View>
           </Modal>
        </Portal>
      ) : null
   }
  
}

LoginScreen.propTypes = {
  googleAuth: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  isLogged: state.isLogged.status,
  profile: state.isLogged.profile,
})

export default connect(mapStateToProps, { googleAuth })(LoginScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoContainer: {
    paddingBottom: '15%',
  },
  loginHeader: {
    alignItems: 'center',
    borderBottomWidth: 3,
    borderStyle: "solid",
    borderBottomColor: Color.tintColor,
    width: '85%',
    paddingBottom: '5%'
  },
  loginHeaderText: {
    fontSize: 16,
    fontFamily: 'Poppins-Regular',
  },
  loginContainer: {
    paddingTop: '10%',
    width: '85%'
  },
  facebookButton: {
    backgroundColor: "#3B5999",
    padding: 10,
    borderRadius: 5,
  },
  googleButton: {
    backgroundColor: "#fff",
    padding: 10,
    borderRadius: 5,
    marginTop: 8,
    elevation: 2,
  },
  facebookButtonText: {
    flex: 1,
    fontSize: 18,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'Poppins-Regular'
  }, 
  googleButtonText: {
    flex: 1,
    fontSize: 18,
    color: 'grey',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: 'Poppins-Regular'
  },
  nestedButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  loading: {
    alignSelf: 'center',
    backgroundColor: '#fff',
    width: '65%',
    borderRadius: 5,
    padding: 15
  },
  loadingText: {
    flex: 1,
    color: 'grey',
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 15,
    fontFamily: 'Poppins-Regular',
  }
});
