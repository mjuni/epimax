import React, { Component } from 'react';
import Layout from '../constants/Layout';
// import { WebView } from 'react-native-webview';
import {
    Text,
    View,
    Dimensions,
    ScrollView,
    StyleSheet,
} from 'react-native';

export default class NotesScreen extends Component {
    render() {
        const {navigation} = this.props
        return (
            // <WebView
            //     style={ Layout.container }
            //     source={{uri: navigation.state.params.notes}}
            //     allowsBackForwardNavigationGestures={true}
            // />
            <ScrollView
                horizontal={true}
                pagingEnabled={true}
                style={styles.readerView}
                showsHorizontalScrollIndicator={false}
            >
                <View style={styles.readerText}>
                    <Text>Hello</Text>
                </View>
                <View style={styles.readerText}>
                    <Text>This is a sample reader for development of reading app.</Text>
                </View>
                <View style={styles.readerText}>
                    <Text>This is a sample reader for development of reading app.</Text>
                </View>
                <View style={styles.readerText}>
                    <Text>This is a sample reader for development of reading app.</Text>
                </View>
                <View style={styles.readerText}>
                    <Text>This is a sample reader for development of reading app.</Text>
                </View>
            </ScrollView>
        )
    }
}


let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;
const styles = StyleSheet.create({
    readerView: {
        flex: 1,
        backgroundColor: '#fff',
    },
    readerText: {
        padding: 25,
        width: width, 
        height:height,
        alignItems: 'center',
    }
});