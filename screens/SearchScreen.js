import React, {Component} from 'react';
import Color from '../constants/Colors';
import capitalizeWords from '../helper functions/CapitalizeWord';
import { 
    Text,
    View,
    FlatList,
    TextInput,
    StyleSheet,
} from "react-native";
import { 
    TouchableRipple
} from "react-native-paper";

export default class SearchScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            topics: [],
            firstQuery: '',
        }
    }
    
    componentDidMount() {
        this.props.navigation.setParams({ 
            handleSearch: this._handleChange.bind(this),
            firstQuery: this._firstQuery,
        });
    }

    _firstQuery() {
        this.setState({
            firstQuery: ''
        })
    }

    _handleChange(value) {
        if(value.length > 0){
            const topics = [
                {
                    id: '1',
                    name: 'Topic 1',
                    description: 'Descrption 1'
                },
                {
                    id: '2',
                    name: 'Topic 2',
                    description: 'Descrption 2'
                },
                {
                    id: '3',
                    name: 'Topic 3',
                    description: 'Descrption 3'
                },
                {
                    id: '4',
                    name: 'Topic 4',
                    description: 'Descrption 4'
                },
                {
                    id: '5',
                    name: 'Topic 5',
                    description: 'Descrption 5'
                },
            ]
            let filters = topics.filter(topic => topic.name.toLowerCase().includes(value.toLowerCase()))
            this.setState({
                topics: filters,
                firstQuery: value
            })
        }else{
           this.setState({
               topics:[],
               firstQuery:value
            })
        }
    }

    render() {
        const state = this.state
        return (
            <View style={styles.container}>
                <FlatList
                    data={state.topics}
                    style={styles.scrolleView}
                    renderItem={({ item }) => 
                    <TouchableRipple onPress={() => this.props.navigation.navigate('Notes', {notes: item.notes})}>
                        <View style={styles.detailsContainer}>
                            <Text style={styles.title}>{capitalizeWords(item.name)}</Text>
                            <Text style={styles.description}>{item.description}</Text>
                        </View>
                    </TouchableRipple>
                    }
                    keyExtractor={item => item.id}
                />
            </View>
        );
    }
}

SearchScreen.navigationOptions = ({navigation}) => {
    const { params = {} } = navigation.state;
    return {
        headerTitle: () => (
            <View style={{flex:1, alignSelf: 'stretch'}}>
                {/* <View style={{flex: 1, alignSelf: 'stretch'}}> */}
                    <TextInput
                        placeholder='Search by topic e.g. algebra'
                        placeholderTextColor='rgba(14,17,17,0.7)'
                        style={{alignSelf: 'stretch'}}
                        autoFocus={true}
                        inlineImageLeft={Platform.OS == 'ios' ? 'ios-search' : 'md-search'}
                        onChangeText={text => params.handleSearch(text)}
                    />
                {/* </View> */}
            </View>
        )
    };
}


const border = {
    width: 0.5,
    color: 'rgba(128,128,128,0.3)',
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        width: '100%',
        marginRight: 'auto',
        marginLeft: 'auto',
    },
    detailsContainer: {
        justifyContent: 'center',
        paddingTop: 18,
        paddingBottom: 18,
        borderBottomWidth: border.width,
        borderBottomColor: border.color,
    },
    title: {
        fontWeight: 'bold',
        paddingBottom: 4,
        fontSize: 18,
    },
    description: {
        color: 'rgba(0,0,0,0.5)',
        fontSize: 14,
    },
    scrolleView: {
        paddingRight: 25,
        paddingLeft: 25,
    },
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
        backgroundColor: Color.tintColor,
    },
    containerNoPadding: {
        flex: 1,
        padding: 0,
    },
    // searchcontainer: {
    //     width: 325,
    // },
    searchInput: {
        backgroundColor: '#E6E8E9',
        padding: 7,
        borderRadius: 4,
        fontSize: 17,
    },
});