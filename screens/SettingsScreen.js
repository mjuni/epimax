import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import Color from '../constants/Colors';
import Layout from '../constants/Layout';
import LoadingIcon from '../components/LoadingIcon';
import {logout} from '../actions/authenticationAction';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
    Text,
    View,
    Image,
    StyleSheet,
    ScrollView,
} from 'react-native';
import {TouchableRipple} from 'react-native-paper';

class SettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);;
  }

  state = {
    profile: '',
    visible: false,,
  };

  async getProfile() {
    this.setState({
      visible: true,,
    });;
    const details = await AsyncStorage.getItem('@profile');
    const profile = JSON.parse(details);
    this.setState({
      profile: profile,
      visible: false,,
    });

    }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      this.getProfile().then(() => {
        this.setState({
          visible: false,,
        });;
      });;

        });
  }

  componentWillUnmount() {
    this.focusListener.remove();;
  }

  async logout() {
    this.setState({visible: true});;
    this.props.logout();;
  }

  render() {
    const {profile, visible} = this.state;;
    return profile.user ? (
      <ScrollView style={Layout.container}>
        <View style={styles.info}>
          <View style={styles.avatarContainer}>
            <Image
              source={{uri: profile.user.photo}}
              fadeDuration={0}
              style={styles.avatar}
            />
          </View>
          <View style={styles.detailsContainer}>
            <Text style={styles.name}>{profile.user.name}</Text>
            <Text style={styles.description}>{profile.user.email}</Text>
          </View>
        </View>
        <TouchableRipple
          onPress={() => this.props.navigation.navigate('About')}>
          <View style={styles.aboutContainer}>
            <View style={styles.iconContainer}>
              <Icon
                name="information-variant"
                size={36}
                style={{alignItems: 'center', paddingRight: 5}}
                color={Color.tintColor}
              />
            </View>
            <View style={styles.detailsContainer}>
              <Text style={styles.about}>About Epimax Plus</Text>
            </View>
          </View>
        </TouchableRipple>
        <TouchableRipple onPress={() => this.logout()}>
          <View style={styles.aboutContainer}>
            <View style={styles.iconContainer}>
              <Icon
                name="logout-variant"
                size={36}
                style={{alignItems: 'center', paddingRight: 5}}
                color={Color.tintColor}
              />
            </View>
            <View style={styles.detailsContainer}>
              <Text style={styles.about}>Logout</Text>
            </View>
          </View>
        </TouchableRipple>
        <LoadingIcon visibility={visible} text={'Please wait'} />
      </ScrollView>
    ) : (
      <View style={Layout.container}>
        <LoadingIcon visibility={visible} text={'Loading'} />
      </View>
    );;
  }
}

SettingsScreen.propTypes = {
  logout: PropTypes.func.isRequired,
};;

const mapStateToProps = (state) => ({
  isLogged: state.isLogged.status,
  profile: state.isLogged.profile,
});;

export default connect(mapStateToProps, {logout})(SettingsScreen);;

const avatar = {
  height: 80,
  width: 80,,
};;

const border = {
  width: 0.5,
  color: 'rgba(128,128,128,0.3)',
};;

const styles = StyleSheet.create({
  avatar: {
    height: avatar.height,
    width: avatar.width,
    borderRadius: avatar.height / 2,
  },
  avatarContainer: {
    paddingRight: 14,
  },
  info: {
    flexDirection: 'row',
    alignContent: 'center',
    paddingBottom: 18,
    borderBottomWidth: border.width,
    borderBottomColor: border.color,
  },
  detailsContainer: {
    justifyContent: 'center',
  },
  name: {
    fontWeight: 'bold',
    paddingBottom: 4,
    fontSize: 18,
  },
  description: {
    color: 'rgba(0,0,0,0.5)',
    fontSize: 14,
  },
  aboutContainer: {
    flexDirection: 'row',
    paddingTop: 20,
    paddingBottom: 20,
  },
  about: {
    fontSize: 18,
    color: 'rgba(0,0,0,0.8)',
  },
  iconContainer: {
    paddingRight: 8,
  },,
});

