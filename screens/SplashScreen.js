import React from 'react';
import {ScrollView, Image, StyleSheet, Text} from 'react-native';
import FastImage from 'react-native-fast-image';
import logo from '../assets/images/Splash/Letsudy.jpg';
import { Layout } from '@ui-kitten/components';

export default function SplashScreen() {
  return (
    <Layout style={styles.container}>
      {/* <Image
        source={require('../assets/images/splash.jpg')}
      /> */}
      <Text>Loading...</Text>
    </Layout>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
