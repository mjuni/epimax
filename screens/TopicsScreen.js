import React, {Component} from 'react';
import Color from '../constants/Colors';
import RefreshIcon from "../components/RefreshIcon";
import firestore from '@react-native-firebase/firestore';
import capitalizeWords from '../helper functions/CapitalizeWord';
import AsyncStorage from '@react-native-community/async-storage';
import { 
    Text,
    View,
    FlatList,
    StyleSheet,
} from 'react-native';
import { 
    TouchableRipple,
    FAB,
    Snackbar
} from "react-native-paper";

export default class TopicsScreen extends Component {
    state = {
        topics: [],
        loading: true,
        visible: false,
        subject: [],
        level: [],
    }

    getTopics() {
        const {navigation} = this.props
        const level_id = navigation.state.params.level_id
        const subject_id = navigation.state.params.subject_id
        if(!navigation.state.params.isDownloaded) {
            const db = firestore();
            db.collection('levelSubjects').doc(level_id).collection('subjects').doc(subject_id).collection('topics').get()
                .then(snapshot => {
                    let topics = [];
                    snapshot.forEach(doc => {
                        if(doc && doc.exists) {
                            let data = doc.data()
                            data['id'] = doc.id
                            data['level_id'] = navigation.state.params.level_id
                            data['subject_id'] = navigation.state.params.subject_id
                            topics.push(data)
                            this.setState({
                                topics: topics,
                                loading: false
                            })
                        }
                    });
                }).catch(err => {
                console.log(err)
                this.setState({
                    loading: false
                })
            });
            console.log('you came from home screen')
        } else {
            console.log('you came from library screen')
        }
    }
    
    download(){
        this.setState({
            loading: true
        })

        let data = {
            '@levels' : this.state.level,

            '@subjects' : this.state.subject,

            '@topics' : this.state.topics,
        }

        let data_items = Object.keys(data)

        data_items.map(key => {
            let value = data[key];
            this._checkData(key, value)
        })

        this.setState({
            loading: false,
        })
    }

    _storeData = async (key, data) => {
        try {
          await AsyncStorage.setItem(key, JSON.stringify(data)).then(() => {
              this.setState({
                visible: true
              })
          });
        } catch (error) {
          console.log(error)
        }
    }

    _checkData = async (key, data) => {
        try {
            let value = await AsyncStorage.getItem(key);
            if (value !== null) {
                value = JSON.parse(value);
                if(key === '@levels') {
                    data.map(val => {
                        let existing_item = value.filter(item => item.id.includes(val.id))
                        if(existing_item.length == 0) {
                            value.push(val)
                        }
                    })
                    await AsyncStorage.setItem(key, JSON.stringify(value))
                } 
                
                if(key === '@subjects') {
                    data.map(async val => {
                        let existing_item = value.filter(item => item.level_id.includes(val.level_id) && item.name.includes(val.name))
                        if(existing_item.length == 0) {
                            value.push(val)
                        }
                    })
                    await AsyncStorage.setItem(key, JSON.stringify(value))
                }

                if(key === '@topics') {
                    data.map(async val => {
                        let existing_item = value.filter(item => item.level_id.includes(val.level_id) && item.name.includes(val.name))
                        if(existing_item.length == 0) {
                            value.push(val)
                        }
                    })
                    await AsyncStorage.setItem(key, JSON.stringify(value)).then(() => {
                        this.setState({
                          visible: true
                        })
                    });
                }
            } else {
                this._storeData(key,data)
            }
        } catch (error) {
            console.log(error)
        }
    }

    async fetchTopics() {
        try {
            const value = await AsyncStorage.getItem('@topics');
            if (value !== null) {
                let topics = JSON.parse(value).filter(topic => topic.level_id === this.props.navigation.state.params.level_id && topic.subject_id === this.props.navigation.state.params.subject_id)
                this.setState({
                    topics: topics
                });
            }
        } catch (error) {
            console.log(error)
        }
    }

    async componentDidMount() {
        this.focusListener = this.props.navigation.addListener("didFocus", () => {
            const {navigation} = this.props
            if(!navigation.state.params.isDownloaded) {
                this.getTopics();
            }else{
                this.fetchTopics();
            }
            if(navigation.state.params.level && navigation.state.params.subject) {
                const subject = [];
                const level = [
                    {
                        id: navigation.state.params.level_id,
                        name: navigation.state.params.level
                    }
                ];
                const subj = navigation.state.params.subject;
                subj.level_id = navigation.state.params.level_id;
                subject.push(subj);
                this.setState({
                    subject: subject,
                    level: level
                });
            }
        });
    }

    componentWillUnmount() {
        this.focusListener.remove()
    }

    render() {
        const {navigation} = this.props
        const state = this.state
        return (
            <View style={styles.container}>
                <FlatList
                    data={state.topics}
                    style={styles.scrolleView}
                    renderItem={({ item }) => 
                    <TouchableRipple onPress={() => this.props.navigation.navigate('Notes', {notes: item.notes})}>
                        <View style={styles.detailsContainer}>
                            <Text style={styles.title}>{capitalizeWords(item.name)}</Text>
                            <Text style={styles.description}>{item.description}</Text>
                        </View>
                    </TouchableRipple>
                    }
                    keyExtractor={item => item.id}
                />
                {
                    !state.loading ? (
                        <FAB
                            style={styles.fab}
                            icon="download"
                            visible={!navigation.state.params.isDownloaded}
                            onPress={() => this.download()}
                        />
                    ) : null
                }
                
                <Snackbar
                    visible={state.visible}
                    onDismiss={() => this.setState({ visible: false })}
                >
                    Subject added to library.
                </Snackbar>
                        
                {
                    !navigation.state.params.isDownloaded ? (
                        state.loading ? (
                            <RefreshIcon />
                        ) : null
                    ) : null
                }
            </View>
        )
    }
}


const border = {
    width: 0.5,
    color: 'rgba(128,128,128,0.3)',
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  detailsContainer: {
      justifyContent: 'center',
      paddingTop: 18,
      paddingBottom: 18,
      borderBottomWidth: border.width,
      borderBottomColor: border.color,
  },
  title: {
      fontWeight: 'bold',
      paddingBottom: 4,
      fontSize: 18,
  },
  description: {
      color: 'rgba(0,0,0,0.5)',
      fontSize: 14,
  },
  scrolleView: {
    paddingRight: 25,
    paddingLeft: 25,
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: Color.tintColor,
  },
});