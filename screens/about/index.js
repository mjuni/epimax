import React, {Component} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {Layout} from '@ui-kitten/components';

export default class AboutScreen extends Component {
  render() {
    return (
      <Layout style={styles.container}>
        <Image
          source={require('../../assets/images/logo.jpeg')}
          style={{height: 50, width: 300}}
        />
        <Text style={styles.version}>Version: 1.0.1</Text>
        <View style={styles.footer}>
          <Text style={styles.footerText}>
            Copyright {'\u00A9'} Epimax Plus All Rights Reserved
          </Text>
        </View>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  logoText: {
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    paddingTop: 25,
  },
  version: {
    alignSelf: 'center',
    fontSize: 16,
    paddingTop: 25,
    color: 'rgb(128,128,128)',
  },
  footer: {
    position: 'absolute',
    bottom: 15,
  },
  footerText: {
    fontSize: 14,
    bottom: 0,
    color: 'rgb(128,128,128)',
  },
});
