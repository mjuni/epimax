import React,{useEffect} from 'react';
import {
  View,
  Dimensions,
  TouchableOpacity,
  FlatList,
  StyleSheet,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {capitalizeWords} from '../../../util';
import {Text} from '@ui-kitten/components';
export default function ({subjects, level, onItemPress}) {
  const handlePress = (subject) => {
    onItemPress(subject)
  };
  return (
    <View style={styles.container}>
      <Text style={styles.classHeader}>{level.name}</Text>
      <FlatList
        data={subjects}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={{marginTop: 15}}
        renderItem={({item}) => (
          <TouchableOpacity
            style={styles.bookItem}
            onPress={() => handlePress(item)}>
            <FastImage
              style={styles.bookCover}
              source={{uri: item.image_url}}
            />
            <Text style={styles.bookTitle}>{capitalizeWords(item.name)}</Text>
          </TouchableOpacity>
        )}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}
const {height} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
    margin: 14,
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%',
    paddingRight: 25,
    paddingLeft: 25,
    marginRight: 'auto',
    marginLeft: 'auto',
    bottom: 0,
  },
  containerSection: {
    paddingTop: 0,
    height: height / 4,
    marginTop: 15,
  },
  classHeader: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  bookSection: {
    flex: 1,
    paddingTop: 15,
    flexDirection: 'row',
  },
  bookCover: {
    width: 130,
    height: height / 4 - 35,
  },
  bookItem: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'flex-end',
    paddingRight: 13,
  },
  bookTitle: {
    paddingTop: 8,
    fontWeight: 'bold',
    fontSize: 15,
    width: 130,
  },
});
