import React from 'react';
import {View} from 'react-native';
import {BottomNavigationTab, Divider} from '@ui-kitten/components';
import {useSafeArea} from 'react-native-safe-area-context';
import {BrandBottomNavigation} from '../../components/brand-bottom-navigation';
import {
  LayoutIcon,
  BookmarkOutline,
  SettingsOutlineIcon,
} from '../../components/icons';

export const HomeBottomNavigation = (props) => {
  const inserts = useSafeArea();
  const onSelect = (index) => {
    props.navigation.navigate(props.state.routeNames[index]);
  };

  return (
    <View style={{paddingTop: inserts.top}}>
      <Divider />
      <BrandBottomNavigation
        appearance="noIndicator"
        selectedIndex={props.state.index}
        onSelect={onSelect}>
        <BottomNavigationTab title="Home" icon={LayoutIcon} />
        <BottomNavigationTab title="Library" icon={BookmarkOutline} />
        <BottomNavigationTab title="Settings" icon={SettingsOutlineIcon} />
      </BrandBottomNavigation>
    </View>
  );
};

