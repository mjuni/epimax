import React, {useEffect} from 'react';
import {Image} from 'react-native';
import Section from './components/Section';
import FastImage from 'react-native-fast-image';
import {ScrollView, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {ArrowBackIcon} from '../../components/icons';
import RefreshIcon from '../../components/RefreshIcon';
import {fetchLevels, fetchSubjects} from '../../slices';
import {
  Layout,
  TopNavigation,
  TopNavigationAction,
} from '@ui-kitten/components';
export default function HomeScreen({navigation}) {
  const dispatch = useDispatch();
  const forms = useSelector((state) => state.level.levels);
  const formLoading = useSelector((state) => state.level.loading);
  const subjects = useSelector((state) => state.subject.subjects);
  const subjectLoading = useSelector((state) => state.subject.loading);
  const handlePress = (subject, level) => {
    navigation.navigate('Topics', {
      subject,
      level,
    });
  };
  useEffect(() => {
    dispatch(fetchLevels());
  }, []);
  useEffect(() => {
    if (subjects.length <= 0 && forms.length > 0) {
      dispatch(fetchSubjects(forms));
    }
  }, [forms, dispatch]);
  if (subjects.length <= 0) {
    return <RefreshIcon />;
  }
  // const renderBackAction = () => (
  //   <TopNavigationAction icon={ArrowBackIcon} onPress={navigation.goBack} />
  // );
  // const renderRightActions = () => <Image source={icon} />;
  return (
    <>
      <Layout level="4" style={{ paddingBottom: 10 }}>
        <TopNavigation title="Home" />
      </Layout>
      <Layout style={{ flex: 1 }} level="1">
        <ScrollView style={Layout.container}>
          {forms.map((level) => (
            <Section
              level={level}
              key={level.id}
              subjects={subjects.filter((sub) => sub.level_id === level.id)}
              onItemPress={(currentSubject) => handlePress(currentSubject, level)}
            />
          ))}
        </ScrollView>
      </Layout>
    </>
  );
}

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    width: 30,
    backgroundColor: '#fff',
    borderRadius: 30 / 2,
    elevation: 4,
    marginTop: 5,
  },
});
