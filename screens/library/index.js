import React from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {
  Layout,
  TopNavigation,
  TopNavigationAction,
} from '@ui-kitten/components';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import Section from '../home/components/Section';
import {ArrowBackIcon} from '../../components/icons';
import RefreshIcon from '../../components/RefreshIcon';
export default function LibraryScreen({}) {
  const navigation = useNavigation();
  const forms = useSelector((state) => state.library.levels);
  const subjects = useSelector((state) => state.library.subjects);
  const topics = useSelector((state) => state.library.topics);
  const handleItemPress = (subject, level) => {
    navigation.navigate('Reader', {
      subject,
      level,
      topics: topics,
    });
  };
  const renderBackAction = () => (
    <TopNavigationAction icon={ArrowBackIcon} onPress={navigation.goBack} />
  );

  return (
    <Layout style={styles.container}>
      <TopNavigation title="Library" accessoryLeft={renderBackAction} />
      <ScrollView>
        {forms.map((level) => (
          <Section
            level={level}
            key={level.id}
            subjects={subjects.filter((sub) => sub.level_id === level.id)}
            onItemPress={(currentSubject) =>
              handleItemPress(currentSubject, level)
            }
          />
        ))}
      </ScrollView>
    </Layout>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    width: 30,
    backgroundColor: '#fff',
    borderRadius: 30 / 2,
    elevation: 4,
    marginTop: 5,
  },
});
