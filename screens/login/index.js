import React from 'react';
import {RectButton} from 'react-native-gesture-handler';
import {GoogleSigninButton} from '@react-native-community/google-signin';
import {useDispatch} from 'react-redux';
import {signinWithGoogle, signinWithFacebook} from '../../slices';
import Color from '../../constant';
import logo from '../../assets/images/logo.jpeg';
import Icon from 'react-native-vector-icons/Ionicons';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

export default function LoginScreen({navigation}) {
  const dispatch = useDispatch();
  const handlePress = async () => {
    try {
      await dispatch(signinWithGoogle());
    } catch (error) {}
  };
  const facebookAuth = async () => {
    try {
      await dispatch(signinWithFacebook());
    } catch (error) {
      console.log(error)
    }
  };
  return (
    <View style={styles.container}>
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            source={logo}
            fadeDuration={0}
            style={{height: 50, width: 300}}
          />
        </View>
        <View style={styles.loginHeader}>
          <Text style={styles.loginHeaderText}>Easy Login</Text>
        </View>
        <View style={styles.loginContainer}>
          <TouchableOpacity onPress={facebookAuth} style={styles.facebookButton}>
            <View style={styles.nestedButtonView}>
              <Icon name="logo-facebook" size={32} color="white" />
              <Text style={styles.facebookButtonText}>Log in with Facebook</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={handlePress} style={styles.googleButton}>
            <View style={styles.nestedButtonView}>
              <Image
                source={require('../../assets/images/google-icon.png')}
                fadeDuration={0}
                style={{width: 30, height: 30}}
              />
              <Text style={styles.googleButtonText}>Log in with Google</Text>
            </View>
          </TouchableOpacity>
          {/* <GoogleSigninButton
            style={{ width: 200, height: 48 }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={handlePress}/> */}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoContainer: {
    paddingBottom: '15%',
  },
  loginHeader: {
    alignItems: 'center',
    borderBottomWidth: 3,
    borderStyle: 'solid',
    borderBottomColor: Color.tintColor,
    width: '85%',
    paddingBottom: '5%',
  },
  loginHeaderText: {
    fontSize: 16,
  },
  loginContainer: {
    paddingTop: '10%',
    width: '85%',
  },
  facebookButton: {
    backgroundColor: '#3B5999',
    padding: 10,
    borderRadius: 5,
  },
  googleButton: {
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 5,
    marginTop: 8,
    elevation: 2,
  },
  facebookButtonText: {
    // flex: 1,
    fontSize: 18,
    color: '#fff',
    textAlign: 'center',
  },
  googleButtonText: {
    flex: 1,
    fontSize: 18,
    color: 'grey',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: 'Poppins-Regular',
  },
  nestedButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  loading: {
    alignSelf: 'center',
    backgroundColor: '#fff',
    width: '65%',
    borderRadius: 5,
    padding: 15,
  },
  loadingText: {
    flex: 1,
    color: 'grey',
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 15,
    fontFamily: 'Poppins-Regular',
  },
});
