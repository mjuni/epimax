import React from 'react';
import {Text} from '@ui-kitten/components';
import {Dimensions, StyleSheet, ScrollView} from 'react-native';

export default function Menu({onItemSelected}) {
  return (
    <ScrollView style={styles.menu}>
      <Text onPress={() => onItemSelected('About')} style={styles.item}>
        About
      </Text>

      <Text onPress={() => onItemSelected('Contacts')} style={styles.item}>
        Contacts
      </Text>
    </ScrollView>
  );
}
const window = Dimensions.get('window');

const styles = StyleSheet.create({
  menu: {
    flex: 1,
    width: window.width,
    height: window.height,
    backgroundColor: 'gray',
    padding: 20,
  },
  item: {
    fontSize: 14,
    fontWeight: '300',
    paddingTop: 5,
  },
});
