import React, {useState, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {Layout} from '@ui-kitten/components';
import BookReader from '../../components/reader/BookReader';

export default function Reader({navigation, route}) {
  return (
    <Layout style={styles.container}>
      <BookReader {...route.params} />
    </Layout>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
