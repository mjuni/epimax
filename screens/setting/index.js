import React from 'react';
import {StyleSheet} from 'react-native';
import {signOutWithGoogle, signOutUser, signOutWithFacebook} from '../../slices';
import FastImage from 'react-native-fast-image';
import {ArrowBackIcon} from '../../components/icons';
import {useSelector, useDispatch} from 'react-redux';
import {
  Layout, 
  Divider,
  Icon,
  ListItem,
  TopNavigation,
  TopNavigationAction,
} from '@ui-kitten/components';

export default function SettingsScreen({navigation}) {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.auth.userInfo.user);
  const provider = useSelector((state) => state.auth.authProvider);
  const logout = (provider) => {
    if (provider == 'google') {
      console.log('google signout');
      dispatch(signOutWithGoogle());
    } else {
      console.log('fb signout');
      dispatch(signOutWithFacebook());
      
    }
  };
  const handleNavigation = () => {
    navigation.navigate('About');
  };
  const renderBackAction = () => (
    <TopNavigationAction icon={ArrowBackIcon} onPress={navigation.goBack} />
  );
  return (
    <Layout style={styles.container} level="1">
      <TopNavigation title="Settings" accessoryLeft={renderBackAction} />
      <ListItem
        accessoryLeft={(props) => (
          <FastImage
            {...props}
            source={{uri: user.photo}}
            style={styles.avatar}
          />
        )}
        title={`${user.name}`}
        description={user.email}
      />
      <Divider />
      <ListItem
        style={{paddingTop: 20}}
        title="About"
        accessoryLeft={(props) => (
          <Icon
            {...props}
            style={styles.icon}
            fill="#8F9BB3"
            name="alert-circle-outline"
          />
        )}
        onPress={handleNavigation}
      />
      <Divider />
      <ListItem
        style={{paddingTop: 20}}
        title="Logout"
        accessoryLeft={(props) => (
          <Icon
            {...props}
            style={styles.icon}
            fill="#8F9BB3"
            name="log-out-outline"
          />
        )}
        onPress={() => logout(provider)}
      />
      <Divider />
    </Layout>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
  },
  icon: {
    width: 20,
    height: 20,
  },
});
