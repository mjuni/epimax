import React, {useEffect} from 'react';
import {ScrollView} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useRoute, useNavigation} from '@react-navigation/native';
import {
  Layout,
  Button,
  Text,
  TopNavigation,
  TopNavigationAction,
} from '@ui-kitten/components';
import {useSelector, useDispatch} from 'react-redux';
import {fetchTopics, addLevel, addSubject, addTopics} from '../../slices';
import {RateBar} from '../../components/RateBar';
import RefreshIcon from '../../components/RefreshIcon';
import {ArrowBackIcon} from '../../components/icons';
import {capitalizeWords} from '../../util';
import {View, FlatList, StyleSheet, TouchableOpacity} from 'react-native';

export default function TopicsScreen() {
  const route = useRoute();
  const dispatch = useDispatch();
  const topics = useSelector((state) => state.topic.topics);
  const loading = useSelector((state) => state.topic.loading);
  const navigation = useNavigation();
  const {subject, level} = route.params;
  const handlePress = (currentTopic, currentTopicIndex) => {
    navigation.navigate('Reader', {
      subject,
      level,
      currentTopic,
      currentTopicIndex,
      topics,
    });
  };
  const renderBackAction = () => (
    <TopNavigationAction icon={ArrowBackIcon} onPress={navigation.goBack} />
  );
  const handleAddToLibrary = () => {
    dispatch(addLevel(level));
    dispatch(addSubject(subject));
    dispatch(addTopics(topics));
  };
  useEffect(() => {
    dispatch(fetchTopics(level.id, subject.id));
  }, [level.id, subject.id, dispatch]);
  return (
    <>
      <Layout level="4" style={{paddingBottom: 10}}>
        <TopNavigation title="Preview" accessoryLeft={renderBackAction}  />
      </Layout>
      <Layout level="1" style={styles.container}>
        <ScrollView>
          <View style={styles.header}>
            <FastImage
              style={styles.productImage}
              source={{uri: subject.image_url}}
            />
            <View style={styles.detailsContainer}>
              <Text category="s1">{subject.name}</Text>
              <View style={styles.categoryContainer}>
                <Button style={styles.categoryItem} size="tiny">
                  {level.name}
                </Button>
              </View>
              {/* <RateBar style={styles.rateBar} value={3} /> */}
            </View>
          </View>
          <Button style={styles.buyButton} onPress={handleAddToLibrary}>
            ADD TO LIBRARY
          </Button>
          <Layout level="2" style={styles.descriptionContainer}>
            {loading ? (
              <RefreshIcon />
            ) : (
              <FlatList
                data={topics}
                style={styles.scrolleView}
                ListHeaderComponent={<Text category="h5">Topics</Text>}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => handlePress(item, index)}>
                    <View style={styles.listItem}>
                      <Text category="h6">{capitalizeWords(item.name)}</Text>
                      <Text appearance="hint">{item.description}</Text>
                    </View>
                  </TouchableOpacity>
                )}
                keyExtractor={(item) => item.id}
              />
            )}
          </Layout>
        </ScrollView>
      </Layout>
    </>
  );
}

const border = {
  width: 0.5,
  color: 'rgba(128,128,128,0.3)',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    overflow: 'hidden',
    padding: 16,
  },
  productImage: {
    width: 140,
    height: 180,
  },
  categoryContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 12,
    marginHorizontal: -4,
  },
  categoryItem: {
    marginHorizontal: 4,
    borderRadius: 16,
  },
  rateBar: {
    marginHorizontal: -4,
  },
  detailsContainer: {
    marginHorizontal: 24,
  },
  listItem: {
    justifyContent: 'center',
    paddingTop: 18,
    paddingBottom: 18,
    borderBottomWidth: border.width,
    borderBottomColor: border.color,
  },
  title: {
    fontWeight: 'bold',
    paddingBottom: 4,
    fontSize: 18,
  },
  description: {
    color: 'rgba(0,0,0,0.5)',
    fontSize: 14,
  },
  scrolleView: {
    paddingRight: 25,
    paddingLeft: 25,
    marginBottom: 20,
  },
  descriptionContainer: {
    margin: 10,
    flex: 1,
  },
  buyButton: {
    marginHorizontal: 16,
    marginVertical: 10,
  },
});
