import AsyncStorage from '@react-native-community/async-storage';
const MAPPING_KEY = 'mapping';
const THEME_KEY = 'theme';
const LEVEL_KEY = '_@levels';
const SUBJECT_KEY = '_@subjects';
const TOPIC_KEY = '_@topics';
export class AppStorage {
  static getMapping = (fallback) => {
    return AsyncStorage.getItem(MAPPING_KEY).then((mapping) => {
      return mapping || fallback;
    });
  };

  static getTheme = (fallback) => {
    return AsyncStorage.getItem(THEME_KEY).then((theme) => {
      return theme || fallback;
    });
  };

  static setMapping = (mapping) => {
    return AsyncStorage.setItem(MAPPING_KEY, mapping);
  };

  static setTheme = (theme) => {
    return AsyncStorage.setItem(THEME_KEY, theme);
  };
  static setLevel = (level) => {
    return AsyncStorage.setItem(LEVEL_KEY, JSON.stringify(level));
  };
  static getLevels = () => {
    return AsyncStorage.getItem(LEVEL_KEY).then((levels) => {
      if (levels) {
        return JSON.parse(levels);
      }
      return [];
    });
  };
  static setTopics = (topic) => {
    return AsyncStorage.setItem(TOPIC_KEY, JSON.stringify(topic));
  };
  static getTopics = () => {
    return AsyncStorage.getItem(TOPIC_KEY).then((topics) => {
      if (topics) {
        return JSON.parse(topics);
      }
      return [];
    });
  };
  static setSubjects = (subject) => {
    return AsyncStorage.setItem(SUBJECT_KEY, JSON.stringify(subject));
  };
  static getSubjects = () => {
    return AsyncStorage.getItem(SUBJECT_KEY).then((subjects) => {
      if (subjects) {
        return JSON.parse(subjects);
      }
      return [];
    });
  };
}
