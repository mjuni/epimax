import firestore from '@react-native-firebase/firestore';
export class FirebaseService {
  static db = firestore();
  static levelRef = this.db.collection('levelSubjects');

  static async fetchLevels() {
    try {
      const list = [];
      const snapshot = await firestore().collection('levelSubjects').get();
      snapshot.forEach((doc) => {
        const {name} = doc.data();
        list.push({id: doc.id, name});
      });
      return list;
    } catch (error) {
      console.log(error);
      return Promise.reject('An Error occured!');
    }
  }
  static async fetchSubjectsByLevel(level_id) {
    try {
      const list = [];
      const snapshot = await firestore()
        .collection('levelSubjects')
        .doc(level_id)
        .collection('subjects')
        .get();
      snapshot.forEach((doc) => {
        if (doc && doc.exists) {
          const data = doc.data();
          list.push({level_id, ...data});
        }
      });
      return Promise.resolve(list);
    } catch (error) {
      return Promise.reject(error);
    }
  }
  static async fetchTopics(level_id, subject_id) {
    let topics = [];
    try {
      const snapshot = await firestore()
        .collection('levelSubjects')
        .doc(level_id)
        .collection('subjects')
        .doc(subject_id)
        .collection('topics')
        .get();
      snapshot.forEach((doc) => {
        if (doc && doc.exists) {
          const data = doc.data();
          topics.push({level_id, subject_id, id: doc.id, ...data});
        }
      });
      return Promise.resolve(topics);
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
