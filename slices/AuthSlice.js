import {createSlice} from '@reduxjs/toolkit';
import authenticate from '@react-native-firebase/auth';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';

const initialState = {
  isAuthenticated: null,
  isLoading: false,
  authProvider: null,
  userInfo: null,
  errors: [],
};

const auth = createSlice({
  initialState: initialState,
  name: 'auth',
  reducers: {
    getCurrentUserStart(state) {
      state.isLoading = true;
      state.isAuthenticated = null;
    },
    getCurrentUserSuccess(state, action) {
      state.isLoading = false;
      state.isAuthenticated = true;
      state.userInfo = action.payload;
    },
    getCurrentUserFailure(state) {
      state.isLoading = false;
      state.isAuthenticated = false;
    },
    signingSuccess(state, action) {
      console.log('Success');
      state.isAuthenticated = true;
      state.isLoading = false;
      state.userInfo = action.payload.userInfo;
      state.authProvider = action.payload.authProvider;
    },
    signOut(state) {
      state.isAuthenticated = false;
      state.userInfo = null;
    },
    signingFailure(state, action) {
      state.errors.push(action.payload);
    },
  },
});

export default auth.reducer;

export const {
  signingSuccess,
  signingFailure,
  signOut,
  getCurrentUserStart,
  getCurrentUserSuccess,
  getCurrentUserFailure,
} = auth.actions;

export const signinWithGoogle = () => async (dispatch) => {
  try {
    await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();
    console.log({userInfo});
    dispatch(signingSuccess({userInfo: userInfo, authProvider: 'google'}));
    // create a new firebase credential with the token
    const credential = authenticate.GoogleAuthProvider.credential(
      userInfo.idToken,
    );
    // login with credential
    await authenticate().signInWithCredential(credential);
  } catch (error) {
    let message = 'An error occured, please try again';
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      message = 'Signing canceled';
    } else if (error.code === statusCodes.IN_PROGRESS) {
      // operation (e.g. sign in) is in progress already
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      message = 'Play services not available or outdated';
      // play services not available or outdated
    } else {
      // some other error happened
      message = error;
    }
    dispatch(signingFailure({message}));
  }
};

export const signinWithFacebook = () => async (dispatch) => {
  console.log('signing in with facebook') ;
  try {
    // Attempt login with permissions
    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);

    if (result.isCancelled) {
      throw 'User cancelled the login process';
    }

    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken();
    const accessToken = data.accessToken;

    if (!data) {
      throw 'Something went wrong obtaining access token';
    }

    //Get user details
    let userInfo = {
      user: {},
    };
    fetch(
      `https://graph.facebook.com/v2.5/me?fields=email,name&access_token=${accessToken}`,
    )
      .then((res) => res.json())
      .then((_data) => {
        userInfo.idToken = accessToken;
        userInfo.user.name = _data.name;
        userInfo.user.id = _data.id;
        userInfo.user.email = _data.email;
        userInfo.user.photo = `https://graph.facebook.com/${_data.id}/picture`;
        dispatch(
          signingSuccess({userInfo: userInfo, authProvider: 'facebook'}),
        );
      });

    // Create a Firebase credential with the AccessToken
    const facebookCredential = authenticate.FacebookAuthProvider.credential(
      accessToken,
    );

    // Sign-in the user with the credential
    return authenticate().signInWithCredential(facebookCredential);
  } catch (error) {
    let message = error;
    console.log(error);
    dispatch(signingFailure({message}));
  }
};

export const getCurrentUser = () => async (dispatch) => {
  dispatch(getCurrentUserStart());
  try {
    const userInfo = await GoogleSignin.signInSilently();
    dispatch(getCurrentUserSuccess(userInfo));
  } catch (error) {
    console.log(error);
    dispatch(getCurrentUserFailure());
    if (error.code === statusCodes.SIGN_IN_REQUIRED) {
      // user has not signed in yet
    } else {
      // some other error
    }
  }
};

export const signOutUser = (authState) => {
  if (authState.authProvider == 'google') {
    signOutWithGoogle();
  } else {
    signOutWithFacebook() ;
  }
};

export const signOutWithGoogle = () => async (dispatch) => {
  try {
    await GoogleSignin.revokeAccess();
    await GoogleSignin.signOut();
    dispatch(signOut());
    signOut();
  } catch (error) {
    console.error(error);
  }
};

export const signOutWithFacebook = () => async (dispatch) => {
  try {
    LoginManager.logOut();
    dispatch(signOut());
  } catch (error) {
    console.error(error);
  }
};
