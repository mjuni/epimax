import {FirebaseService} from '../services/firebase-service';
import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  levels: [],
  loading: false,
  error: [],
};

const level = createSlice({
  name: 'level',
  initialState: initialState,
  reducers: {
    getLevelsStart(state) {
      state.loading = true;
    },
    getLevelsSuccess(state, action) {
      state.loading = false;
      state.levels = action.payload;
    },
    getLevelsFailure(state, action) {
      state.loading = false;
    },
  },
});

export const {
  getLevelsStart,
  getLevelsSuccess,
  getLevelsFailure,
} = level.actions;
export default level.reducer;

export const fetchLevels = () => async (dispatch) => {
  dispatch(getLevelsStart());
  try {
    const levels = await FirebaseService.fetchLevels();
    dispatch(getLevelsSuccess(levels));
  } catch (error) {
    dispatch(getLevelsFailure());
  }
};
