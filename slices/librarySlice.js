import {createSlice} from '@reduxjs/toolkit';
import uniqBy from 'lodash/unionBy';
const initialState = {
  subjects: [],
  levels: [],
  topics: [],
};

const library = createSlice({
  initialState: initialState,
  name: 'library',
  reducers: {
    addSubject(state, action) {
      state.subjects = uniqBy([...state.subjects, action.payload], 'id');
    },
    addLevel(state, action) {
      state.levels = uniqBy([...state.levels, action.payload], 'id');
    },
    addTopics(state, action) {
      state.topics = uniqBy([...state.topics, ...action.payload], 'id');
    },
  },
});

export default library.reducer;

export const {addLevel, addSubject, addTopics} = library.actions;
