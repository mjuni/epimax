import {FirebaseService} from '../services/firebase-service';
import {createSlice} from '@reduxjs/toolkit';
import flat from 'core-js-pure/features/array/flat';
const {fetchSubjectsByLevel} = FirebaseService;

const initialState = {
  subjects: [],
  loading: false,
  error: [],
};

const subject = createSlice({
  name: 'subject',
  initialState: initialState,
  reducers: {
    getSubjectsStart(state) {
      state.loading = true;
    },
    getSubjectsSuccess(state, action) {
      state.loading = false;
      state.subjects = action.payload.subjects;
    },
    getSubjectSubjectsFailure(state, action) {
      state.loading = false;
    },
  },
});

export const {
  getSubjectsStart,
  getSubjectsSuccess,
  getSubjectSubjectsFailure,
} = subject.actions;
export default subject.reducer;

export const fetchSubjects = (levels) => async (dispatch) => {
  dispatch(getSubjectsStart());
  let requests = levels.map((level) => fetchSubjectsByLevel(level.id));
  Promise.all(requests)
    .then((subjects) => {
      subjects = flat(subjects, 2);
      dispatch(getSubjectsSuccess({subjects}));
    })
    .catch((error) => {
      dispatch(getSubjectSubjectsFailure());
    });
};
