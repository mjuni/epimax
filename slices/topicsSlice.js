import {FirebaseService} from '../services/firebase-service';
import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  topics: [],
  loading: false,
  error: [],
};

const topic = createSlice({
  name: 'topic',
  initialState: initialState,
  reducers: {
    getTopicsStart(state) {
      state.loading = true;
    },
    getTopicsSuccess(state, action) {
      state.loading = false;
      state.topics = action.payload;
    },
    getTopicsFailure(state, action) {
      state.loading = false;
    },
  },
});

export const {
  getTopicsStart,
  getTopicsSuccess,
  getTopicsFailure,
} = topic.actions;
export default topic.reducer;

export const fetchTopics = (level_id, subject_id) => async (dispatch) => {
  dispatch(getTopicsStart());
  try {
    const topics = await FirebaseService.fetchTopics(level_id, subject_id);
    dispatch(getTopicsSuccess(topics));
  } catch (error) {
    dispatch(getTopicsFailure());
  }
};
